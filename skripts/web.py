from flask import Flask, request, send_file
from skripts.dbService import getAllCoordinates, getCoordinateById, saveNewCoord, getFotoById, getAllFotos, saveNewFoto
from skripts.model import Coordinates, Foto
import io


app = Flask(__name__)


@app.route('/coordinates/all' , methods = ["GET"])
def get_all_coordinates():
    result = getAllCoordinates()
    ret = ""
    for i in result:
        ret=ret+i.toDict().__str__()
    return ret

@app.route("/coordinates", methods=[ 'GET'])
def get_coord():
    id = request.args.get('id')
    coord = getCoordinateById(id)
    return coord.toDict().__str__()


@app.route("/coordinates", methods=['POST'])
def save_coordinates():

    latitude = request.form.get("Latitude")
    longitude = request.form.get("Longitude")

    coordinates = Coordinates()
    coordinates.latitude = latitude
    coordinates.longitude = longitude

    saveNewCoord(coordinates)

    return "OK"

@app.route('/foto', methods=['POST'])
def save_foto():

    coordinatesId = request.form.get("coordinatesId")
    foto = request.files.get("file")
    code = request.form.get("code")

    newFoto = Foto()
    newFoto.setFotoData(foto.read())
    newFoto.coordinatesId = coordinatesId
    newFoto.code = code
    saveNewFoto(newFoto)
    return 'OK'


@app.route('/foto', methods=['GET'])
def get_foto():
    id = request.args.get('id')

    returnFoto = getFotoById(id)

    return send_file(
        io.BytesIO(returnFoto.fotoData.fotoData),
        attachment_filename='logo.jpeg',
        mimetype='image/jpeg')


@app.route('/foto/all' , methods = ["GET"])
def get_all_fotoss():
    result = getAllFotos()
    ret = ""
    for i in result:
        ret=ret+i.toDict().__str__()
    return ret


if __name__ == '__main__':
    app.run()