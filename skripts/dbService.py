# import sqlite3
# conn = sqlite3.connect('/Users/martin/PycharmProjects/sqlite/ozs.db')
#
# c = conn.cursor()
# c.execute('SELECT * FROM Coordinates')
# print(c.fetchall())
# conn.close()

from sqlalchemy import create_engine, Float, LargeBinary
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Column, Integer, ForeignKey, String
from sqlalchemy.orm import relationship


import os

from skripts.model import *

if os.path.exists("/Users/martin"):
    engine = create_engine('sqlite:////Users/martin/PycharmProjects/sqlite/ozs.db', echo=True)
else:
    path = "C:\\Users\\mbu\\Documents\\ozs\\sqlite-tools-win32-x86-3270200\\test.db"

    engine = create_engine('sqlite:///'+path, echo=True)

Base = declarative_base(engine)


########################################################################
class DbCoordinates(Base):
    """"""
    __tablename__ = 'Coordinates'
    id = Column("Id",Integer, primary_key=True)
    latitude = Column("Latitude", Float, nullable=False )
    longitude = Column("Longitude", Float, nullable=False )

    def __repr__(self):
        return "<Coordinates (id={}, longitude = {}, latitude = {})>".format(self.id, self.longitude, self.latitude)



class DbPlaceNames(Base):
    """"""
    __tablename__ = 'Place_Names'
    id = Column("Id",Integer, primary_key=True)
    name = Column("Name", String, nullable=False )
    lang = Column("Language", String, nullable=False )

    def __repr__(self):
        return "<Place Name (id={}, Name = {}, Language ={})>".format(self.id, self.name, self.lang)

class DbTags(Base):
    """"""
    __tablename__ = 'Tags'
    id = Column("Id",Integer, primary_key=True)
    tag = Column("Tag", String, nullable=False )

    def __repr__(self):
        return "<Tag (id={}, Tag = {})>".format(self.id, self.tag)

class DbComments(Base):
    """"""
    __tablename__ = 'Comments'
    id = Column("Id",Integer, primary_key=True)
    comment = Column("Comment", String, nullable=False )
    lang = Column("Language", String, nullable=False )

    def __repr__(self):
        return "<Comment (id={}, Comment = {}, Language ={})>".format(self.id, self.comment, self.lang)

class DbFotoData(Base):
    """"""
    __tablename__ = 'Foto_data'
    id = Column("Id",Integer, primary_key=True)
    fotoData = Column("Foto_data", LargeBinary, nullable=False )


    def __repr__(self):
        return "<Place_Names (id={})>".format(self.id)

class DbFoto(Base):
    """"""
    __tablename__ = 'Fotos'

    id = Column("Id",Integer, primary_key=True)
    code = Column("Code", String)
    coordinatesId = Column("Coordinates_Id",Integer, ForeignKey('Coordinates.Id'))
    coordinates = relationship("DbCoordinates")
    fotoDataId = Column("Foto_data_id",Integer, ForeignKey('Foto_data.Id'))
    fotoData = relationship("DbFotoData")
    placeNames = relationship(
        DbPlaceNames,
        secondary='Fotos_Place_names'
    )
    tags = relationship(
        DbTags,
        secondary='Fotos_Tags'
    )
    comments = relationship(
        DbComments,
        secondary='Fotos_Comments'
    )

    def __repr__(self):
        return "<DB Foto (id={}, code = {}, Coordinates ={}, FotoData = {}, Tags = {}, Comments = {})>"\
            .format(self.id, self.code, self.coordinates.__repr__(), self.fotoData.__repr__(), self.tags, self.comments)



class DbFotosPlaceNamesLink(Base):
    """"""
    __tablename__ = 'Fotos_Place_names'
    id = Column("Id", Integer, primary_key=True)
    Fotos_Id = Column("Fotos_Id",Integer, ForeignKey('Fotos.Id'))
    Place_Name_Id = Column("Place_Name_Id",Integer, ForeignKey('Place_Names.Id'))

class DbFotosTagsLink(Base):
    """"""
    __tablename__ = 'Fotos_Tags'
    id = Column("Id", Integer, primary_key=True)
    Fotos_Id = Column("Fotos_Id",Integer, ForeignKey('Fotos.Id'))
    Tag_Id = Column("Tag_Id",Integer, ForeignKey('Tags.Id'))


class DbFotosCommentsLink(Base):
    """"""
    __tablename__ = 'Fotos_Comments'
    id = Column("Id", Integer, primary_key=True)
    Fotos_Id = Column("Fotos_Id",Integer, ForeignKey('Fotos.Id'))
    Comment_Id = Column("Comment_Id",Integer, ForeignKey('Comments.Id'))

# ----------------------------------------------------------------------
def loadSession():
    """"""
    metadata = Base.metadata
    Session = sessionmaker(bind=engine)
    session = Session()
    return session

### Coordinates section

def getAllCoordinates():
    session = loadSession()
    result = []
    dbCoordinatesList = session.query(DbCoordinates).all()
    for dbCoord in dbCoordinatesList:
        newCoord = Coordinates()
        newCoord.id=dbCoord.id
        newCoord.latitude = dbCoord.latitude
        newCoord.longitude = dbCoord.longitude
        result.append(newCoord)

    return result

def getCoordinateById(id):
    session = loadSession()
    dbCoordinate = session.query(DbCoordinates).get(id)

    newCoord = Coordinates()
    newCoord.id=dbCoordinate.id
    newCoord.latitude = dbCoordinate.latitude
    newCoord.longitude = dbCoordinate.longitude

    return newCoord

def saveNewCoord(coordinate):
    session = loadSession()
    newDbCoord = DbCoordinates()
    newDbCoord.latitude = coordinate.latitude
    newDbCoord.longitude = coordinate.longitude
    session.add(newDbCoord)

    session.commit()

### Tag section

def getAllTags():
    session = loadSession()
    result = []
    dbTagsList = session.query(DbTags).all()
    for dbTag in dbTagsList:
        newTag = Tag()
        newTag.id=dbTag.id
        newTag.tag = dbTag.tag
        result.append(newTag)

    return result

def getTagById(id):
    session = loadSession()
    dbTag = session.query(DbTags).get(id)

    newTag = Tag()
    newTag.id = dbTag.id
    newTag.tag = dbTag.tag

    return newTag

def saveNewTag(tag):
    session = loadSession()
    newDBTag = DbTags()
    newDBTag.id = tag.id
    newDBTag.tag = tag.tag
    session.add(newDBTag)

    session.commit()

# Foto section

def saveNewFoto(foto):

    newDbFoto = DbFoto()
    newDbFoto.code = foto.code
    newDbFoto.coordinatesId = foto.coordinatesId
    newDbFoto.fotoData = DbFotoData()
    newDbFoto.fotoData.fotoData = foto.fotoData.fotoData

    session = loadSession()
    session.add(newDbFoto)
    session.commit()


def getFotoById(id):
    session = loadSession()
    dbFoto = session.query(DbFoto).get(id)

    newFoto = Foto()
    newFoto.id = dbFoto.id
    newFoto.code = dbFoto.code

    newFoto.coordinates = Coordinates()
    newFoto.coordinates.id = dbFoto.coordinates.id
    newFoto.coordinates.latitude = dbFoto.coordinates.latitude
    newFoto.coordinates.longitude = dbFoto.coordinates.longitude

    newFoto.placeNames = []
    for dbPlaceName in dbFoto.placeNames:
        newPlaceName = PlaceName()
        newPlaceName.id = dbPlaceName.id
        newPlaceName.name = dbPlaceName.name
        newPlaceName.lang = dbPlaceName.lang
        newFoto.placeNames.append(newPlaceName)

    newFoto.comments = []
    for dbComment in dbFoto.comments:
        newComment = Comment()
        newComment.id = dbComment.id
        newComment.comment = dbComment.comment
        newComment.lang = dbComment.lang
        newFoto.comments.append(newComment)

    newFoto.tags = []
    for dbTak in dbFoto.tags:
        newTag = Tag()
        newTag.id = dbTak.id
        newTag.tag = dbTak.tag
        newFoto.tags.append(newTag)

    newFoto.fotoData = FotoData()
    newFoto.fotoData.id = dbFoto.fotoData.id
    newFoto.fotoData.fotoData = dbFoto.fotoData.fotoData

    return newFoto


def getAllFotos():
    session = loadSession()
    result = []
    dbFotoList = session.query(DbFoto).all()
    for dbFoto in dbFotoList:
        newFoto = Foto()
        newFoto.id=dbFoto.id
        newFoto.code = dbFoto.code


        newFoto.coordinates = Coordinates()
        newFoto.coordinates.id = dbFoto.coordinates.id
        newFoto.coordinates.latitude = dbFoto.coordinates.latitude
        newFoto.coordinates.longitude = dbFoto.coordinates.longitude

        newFoto.placeNames = []
        for dbPlaceName in dbFoto.placeNames:
            newPlaceName = PlaceName()
            newPlaceName.id = dbPlaceName.id
            newPlaceName.name = dbPlaceName.name
            newPlaceName.lang = dbPlaceName.lang
            newFoto.placeNames.append(newPlaceName)

        newFoto.comments = []
        for dbComment in dbFoto.comments:
            newComment= Comment()
            newComment.id = dbComment.id
            newComment.comment = dbComment.comment
            newComment.lang = dbComment.lang
            newFoto.comments.append(newComment)

        newFoto.tags = []
        for dbTak in dbFoto.tags:
            newTag= Tag()
            newTag.id = dbTak.id
            newTag.tag = dbTak.tag
            newFoto.tags.append(newTag)

        newFoto.fotoData = FotoData()
        newFoto.fotoData.id = dbFoto.fotoData.id
        newFoto.fotoData.fotoData = dbFoto.fotoData.fotoData

        result.append(newFoto)

    return result




if __name__ == "__main__":
    session = loadSession()

    res = session.query(DbFoto).all()
    print(res)

    res[0].code = "ABCD1234"

    res = session.query(DbFoto).all()
    print(res)

    session.commit()



    # newCoord = DbCoordinates()
    # newCoord.latitude = 1111.99
    # newCoord.longitude = 111.99
    #
    # newName1 = DbPlace_Names()
    # newName1.name = "kasava_CZ"
    # newName1.lang = "CZE"
    #
    # newName2 = DbPlace_Names()
    # newName2.name = "kasava_EN"
    # newName2.lang = "ENG"
    #
    # newFoto = DbFoto()
    # newFoto.coordinates = newCoord
    # newFoto.placeNames = [newName1, newName2]
    # session.add(newFoto)
