from skripts.model import Coordinates
import sys

def getCloserCoordinates(coordinate, coordinates):



    min = sys.maxsize
    mid = sys.maxsize
    max = sys.maxsize

    result =  [None, None, None]

    for coord in coordinates():
        dist = coordinate.getDistTo(coord)
        if dist < min:
            min = dist
            result[0] = coord
        elif dist < mid:
            mid = dist
            result[1] = coord
        elif dist < max:
            max = dist
            result[2] = coord