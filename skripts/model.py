from geopy.distance import geodesic


class Coordinates:

    def __repr__(self):
        return "<Coordinates (id={}, longitude = {}, latitude = {})>".format(self.id, self.longitude, self.latitude)

    def toDict(self):
        result = {'Id': self.id, 'Latitude': self.latitude, 'Longitude': self.longitude}
        return result

    # TODO - improove performence
    def getDistTo(self, coordinates):
        x1 = (self.latitude, self.longitude)
        x2 = (coordinates.latitude, coordinates.longitude)
        return geodesic(x1, x2).meters


class PlaceName():

    def __repr__(self):
        return "<Place Name (id={}, Name = {}, Language ={})>".format(self.id, self.name, self.lang)

    def toDict(self):
        result = {'Id': self.id, 'Name': self.name, 'Language': self.lang}
        return result


class Tag():

    def __repr__(self):
        return "<Tag (id={}, Tag = {})>".format(self.id, self.tag)

    def toDict(self):
        result = {'Id': self.id, 'Tag': self.tag}
        return result


class Comment():

    def __repr__(self):
        return "<Comment (id={}, Comment = {}, Language ={})>".format(self.id, self.comment, self.lang)

    def toDict(self):
        result = {'Id': self.id, 'Comment': self.comment, 'Language': self.lang}
        return result


class FotoData():

    def toDict(self):
        result = {'Id': self.id}
        return result

    def __repr__(self):
        return "<Place_Names (id={})>".format(self.id)


class Foto:

    def __repr__(self):
        return "<Foto (id={}, code = {}, Coordinates ={}, FotoData = {}, Tags = {}, Comments = {})>" \
            .format(self.id, self.code, self.coordinates.__repr__(), self.fotoData.__repr__(), self.tags, self.comments)

    def toDict(self):
        result = {'Id': self.id, 'Code': self.code, 'Coordinates': self.coordinates.toDict(),
                  'FotoData': self.fotoData.toDict(), 'Tags': [tag.toDict() for tag in self.tags],
                  'Comments': [comment.toDict() for comment in self.comments]}
        return result

    def setFotoData(self, data):
        self.fotoData = FotoData()
        self.fotoData.fotoData = data
