
pragma foreign_keys = ON;

DROP TABLE Foto_data;
--DROP TABLE Fotos_Place_name;
--DROP TABLE Coordinates;
--DROP TABLE Place_names;

--START
CREATE TABLE Coordinates (
Id integer PRIMARY KEY AUTOINCREMENT NOT NULL,
Latitude float(10,6)NOT NULL,
Longitude float(10,6)NOT NULL
);

CREATE TABLE Fotos (
Id integer PRIMARY KEY AUTOINCREMENT NOT NULL,
Code varchar(32),
Foto_data_Id integer NOT NULL ,
Coordinates_Id integer NOT NULL ,
FOREIGN KEY(Foto_data_Id) REFERENCES Foto_data(Id),
FOREIGN KEY(Coordinates_Id) REFERENCES Coordinates(Id)
);

CREATE TABLE Foto_data (
Id integer PRIMARY KEY AUTOINCREMENT NOT NULL,
Foto_data blob
);

CREATE TABLE Place_names (
Id integer PRIMARY KEY AUTOINCREMENT NOT NULL,
Name varchar(60),
Language varchar(3)
);

CREATE TABLE Comments (
Id integer PRIMARY KEY AUTOINCREMENT NOT NULL,
Comment varchar(160),
Language varchar(3)
);

CREATE TABLE Tags (
Id integer PRIMARY KEY AUTOINCREMENT NOT NULL,
Tag varchar(10)
);

CREATE TABLE Fotos_Place_names (
Id integer PRIMARY KEY AUTOINCREMENT NOT NULL,
Fotos_Id integer ,
Place_Name_Id integer,
FOREIGN KEY(Fotos_Id) REFERENCES Fotos(Id),
FOREIGN KEY(Place_Name_Id) REFERENCES Place_names(Id)
);

CREATE TABLE Fotos_Comments (
Id integer PRIMARY KEY AUTOINCREMENT NOT NULL,
Fotos_Id integer ,
Comment_Id integer,
FOREIGN KEY(Fotos_Id) REFERENCES Fotos(Id),
FOREIGN KEY(Comment_Id) REFERENCES Comments(Id)
);

CREATE TABLE Fotos_Tags (
Id integer PRIMARY KEY AUTOINCREMENT NOT NULL,
Fotos_Id integer ,
Tag_Id integer,
FOREIGN KEY(Fotos_Id) REFERENCES Fotos(Id),
FOREIGN KEY(Tag_Id) REFERENCES Tags(Id)
);

