--DELETE

DELETE from Fotos_Place_names;
DELETE from Place_names;
DELETE from Fotos;
DELETE from Coordinates;


--INSERT



INSERT INTO Place_names (Id, Name, Language) VALUES (1, "Snezka", "CZE");
INSERT INTO Place_names (Id, Name, Language) VALUES (2, "SnowHill", "ENG");
INSERT INTO Tags (Id, Tag) VALUES (1, "Husty");
INSERT INTO Tags (Id, Tag) VALUES (2, "Mega");
INSERT INTO Comments (Id, Comment, Language) VALUES (1, "Lorem ipsum dolor sit amet ...", "CZE");
INSERT INTO Coordinates (Id, Latitude, Longitude) VALUES (1, 22.5687458, 22.598745);
INSERT INTO Foto_data (Id, Foto_data) VALUES (1, null);

INSERT INTO Fotos (Id, Code, Foto_data_Id, Coordinates_Id) VALUES (1,"XYZ_123",1,1);

INSERT INTO Fotos_Place_names (Fotos_Id, Place_name_Id) VALUES (1,1);
INSERT INTO Fotos_Place_names (Fotos_Id, Place_name_Id) VALUES (1,2);

INSERT INTO Fotos_Tags (Fotos_Id, Tag_Id) VALUES (1,1);
INSERT INTO Fotos_Tags (Fotos_Id, Tag_Id) VALUES (1,2);

INSERT INTO Fotos_Comments (Fotos_Id, Comment_Id) VALUES (1,1);

--SELECT

SELECT f.Id, f.Code, c.Latitude, c.Longitude, p.Name, t.Tag, d.id
FROM Fotos f, Place_names p, Tags t
INNER JOIN coordinates c ON f.Coordinates_Id = c.Id
INNER JOIN Foto_data d ON f.foto_data_id = d.Id
INNER JOIN Fotos_Place_names fp ON fp.Fotos_Id = f.Id AND fp.Place_Name_Id = p.Id
INNER JOIN Fotos_Tags ft ON ft.Fotos_Id = f.Id AND ft.Tag_Id = t.Id
WHERE f.id = 1 AND p.Language = 'ENG';
